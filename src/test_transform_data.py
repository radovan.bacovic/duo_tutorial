import pytest
from transform_data import download_data, load_json, save_to_csv, convert_to_csv
import json
@pytest.fixture
def sample_data():
    return [
        {'ID': 1, 'FIRST_NAME': 'John', 'LAST_NAME': 'Doe', 'COUNTRY': 'USA'},
        {'ID': 2, 'FIRST_NAME': 'Jane', 'LAST_NAME': 'Smith', 'COUNTRY': 'Canada'},
        {'ID': 3, 'FIRST_NAME': 'Bob', 'LAST_NAME': 'Johnson', 'COUNTRY': 'USA'},
        {'ID': 4, 'FIRST_NAME': 'Alice', 'LAST_NAME': 'Williams', 'COUNTRY': 'UK'}
    ]

def test_download_data(tmp_path):
    file_name = tmp_path / 'test_users.json'
    download_data(str(file_name))
    assert file_name.exists()

def test_load_json(sample_data):
    with pytest.raises(FileNotFoundError):
        load_json('non_existent_file.json')

    file_name = 'test_users.json'
    with open(file_name, 'w') as f:
        json.dump(sample_data, f)

    data = load_json(file_name)
    assert data == sample_data

def test_convert_to_csv(sample_data):
    rows = convert_to_csv(sample_data)
    expected_rows = [
        [1, 'John', 'Doe', 'USA'],
        [2, 'Jane', 'Smith', 'Canada'],
        [3, 'Bob', 'Johnson', 'USA'],
        [4, 'Alice', 'Williams', 'UK']
    ]
    assert rows == expected_rows

def test_save_to_csv(tmp_path, sample_data):
    file_name = tmp_path / 'test_output.csv'
    rows = convert_to_csv(sample_data)
    save_to_csv(rows, str(file_name))
    assert file_name.exists()

# def test_load_data(tmp_path, sample_data):
#     file_name = tmp_path / 'test_output.csv'
#     rows = convert_to_csv(sample_data)
#     save_to_csv(rows, str(file_name))
#
#     with pytest.raises(duckdb.InvalidInputException):
#         load_data('non_existent_file.csv')
#
#     load_data(str(file_name))

# generate test case for load_data function

