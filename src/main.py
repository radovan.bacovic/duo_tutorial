# Generate Fast API application with default endpoint to return message "Hello DSCAdria!" in JSON format
from fastapi import FastAPI
from pydantic import BaseModel, Field
import random
from faker import Faker

app = FastAPI()
fake = Faker()


# Define User model with validation rules
class User(BaseModel):
    id: int = Field(..., example=1)
    first_name: str = Field(..., min_length=1, max_length=50, example="John")
    last_name: str = Field(..., min_length=1, max_length=50, example="Doe")
    country: str = Field(..., min_length=2, max_length=150, example="Germany")


# Default endpoint
@app.get("/")
def root():
    return {"message": "Hello DSCAdria!"}


# Function to generate fake user data
def generate_fake_user() -> User:
    # generate docstring for the function
    """Generate fake user data.

    Returns:
        User: A fake user data object.
    """

    # generate fake user data
    first_name = fake.first_name()
    last_name = fake.last_name()
    country = fake.country()
    user_id = random.randint(1, 1000)
    return User(id=user_id, first_name=first_name, last_name=last_name, country=country)



# Endpoint to return 100 records with fake user data
@app.get("/getUser", response_model=list[User])
def get_users():
    return [generate_fake_user() for _ in range(10000)]
