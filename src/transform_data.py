import requests
import json
import csv
import duckdb


def download_data(file_name="users.json"):
    """Download data from an endpoint and save it as a JSON file.

    Args:
        file_name (str, optional): The name of the JSON file. Defaults to 'users.json'.

    Example:
        download_data('users.json')
    """
    url = "http://127.0.0.1:8000/getUser"
    response = requests.get(url)
    response.raise_for_status()  # Raise an exception for non-2xx status codes
    data = response.json()

    with open(file_name, "w") as f:
        json.dump(data, f)


def load_json(file_name="users.json"):
    """Load JSON data from a file.

    Args:
        file_name (str, optional): The name of the JSON file. Defaults to 'users.json'.

    Returns:
        list: The data loaded from the JSON file.
    """
    with open(file_name, "r") as f:
        data = json.load(f)
    return data


def convert_to_csv(input_json):
    """Flatten a list of dictionaries into a list of lists (rows).

    Args:
        input_json (list): A list of dictionaries to flatten.

    Returns:
        list: A list of lists, where each inner list represents a row.
    """
    if not input_json:
        return []

    keys = input_json[0].keys()
    rows = [[item[key] for key in keys] for item in input_json]
    return rows


def save_to_csv(rows, file_name="output.csv"):
    """Save a list of rows to a CSV file.

    Args:
        rows (list): A list of lists, where each inner list represents a row.
        file_name (str, optional): The name of the CSV file. Defaults to 'output.csv'.
    """
    with open(file_name, "w", newline="") as f:
        writer = csv.writer(f)
        writer.writerow(["ID", "FIRST_NAME", "LAST_NAME", "COUNTRY"])
        writer.writerows(rows)


def load_data(file_name="output.csv"):
    """Load data from a CSV file into a DuckDB table and print top countries by user count.

    Args:
        file_name (str, optional): The name of the CSV file. Defaults to 'output.csv'.
    """
    conn = duckdb.connect()
    conn.execute(f"CREATE TABLE USERS AS SELECT * FROM read_csv_auto('{file_name}');")
    conn.execute(
        "CREATE VIEW TOP_COUNTRIES AS SELECT COUNTRY, COUNT(*) AS NUM_USERS FROM USERS GROUP BY COUNTRY ORDER BY NUM_USERS DESC LIMIT 5;"
    )

    result = conn.execute("SELECT * FROM TOP_COUNTRIES;").fetchall()
    for row in result:
        print(row)

    conn.close()


def transform_data(input_file="users.json", output_file="output.csv"):
    """Transform data by downloading, converting, and loading into DuckDB.

    Args:
        input_file (str, optional): The name of the input JSON file. Defaults to 'users.json'.
        output_file (str, optional): The name of the output CSV file. Defaults to 'output.csv'.
    """
    try:
        download_data(input_file)
        data = load_json(input_file)
        rows = convert_to_csv(data)
        save_to_csv(rows, output_file)
        load_data(output_file)
        print("Transformation completed")
    except Exception as e:
        print(f"Error: {e}")


if __name__ == "__main__":
    transform_data()

# Generate test cases using pytest library for functions
# download_data
# load_json
# convert_to_csv
# save_to_csv
# load_data
