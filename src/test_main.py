import pytest  # noqa: F401
from fastapi.testclient import TestClient

from src_solved.main import User, app

client = TestClient(app)


def test_root():
    response = client.get("/")
    assert response.status_code == 200
    assert response.json() == {"message": "Hello DSCAdria!"}


def test_get_users():
    response = client.get("/getUser")
    assert response.status_code == 200
    users = response.json()
    assert len(users) == 100

    # Check if each user object conforms to the User model
    for user in users:
        try:
            User(**user)
        except ValueError:
            assert False, f"User object {user} is invalid"


def test_user_model():
    # Test valid user data
    valid_user = {
        "id": 123,
        "first_name": "John",
        "last_name": "Doe",
        "country": "United States",
    }
    user = User(**valid_user)
    assert user.id == 123
    assert user.first_name == "John"
    assert user.last_name == "Doe"
    assert user.country == "United States"

    # Test invalid user data
    invalid_user = {
        "id": "invalid",
        "first_name": "X" * 51,
        "last_name": "Y" * 51,
        "country": "Z" * 51,
    }
    try:
        User(**invalid_user)
        assert False, "Invalid user data should raise a ValueError"
    except ValueError:
        pass
