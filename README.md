# 🏁 GitLab Duo tutorial - Intro

## 🎥 [Recorded session](https://youtu.be/Muvn6cPJrmE)

The GitLab Duo tutorials provide practical examples for using GitLab Duo's AI-powered features to assist with software development challenges. 
They cover getting started with a Python project, refactoring existing code, debugging problems, resolving security vulnerabilities, and using DevSecOps best practices.

The tutorials demonstrate GitLab Duo's capabilities through Code Suggestions for writing code more efficiently, Chat for answering development questions, and Vulnerability Explanation for understanding and fixing vulnerabilities.

## What to expect, what not to expect

### ✅ What to expect
- Funny and simple data tutorial with best DevSecOps practices 
- Showcase of how AI tool can be our trusted (really?) buddy in coding work
- Challenge the power of AI in problems we are solving everyday

### ❌ What not to expect
- Any kind of magic to sort out our daily problems
- Fully automated "self-driving" development process
- Boring approach to coding and prompting only, everyone knows that 

## A “Modern” Data App 

## DevSecOps best practices

![devsecops_practices.png](images%2Fdevsecops_practices.png)


## Our journey

![LogoDuo.png](images%2FLogoDuo.png)

Can be used in:
1. GitLab built-in functionality
2. WebIDE
2. Visual Studio Code
3. PyCharm 
4. Few other tools

# 🎥 Scenario

Generate scenario using Summary in the issue [**GitLab Duo Tutorial sample issue**](https://gitlab.com/gitlab-data/analytics/-/issues/20653):
```bash
Describe to me a scenario to test the GitLab duo. 
Want to use a small Web App with a restful API endpoint User and pipeline to transform the data? Data should be stored in
duckdb. Also, include testing, automation and security features 
```
![suggestions.png](images%2Fsuggestions.png)

```mermaid
---
title: Create a data pipeline
---
flowchart TD
    subgraph Web application
        HELLO[Hello World]
        Users[Users]
    end
    
    subgraph ELT/ETL
        Extract[Extract data from the app]
        Transform[Transform data]
        Load[Load data DuckDb]
        Expose[Show data]
    end
    
    subgraph Test 
        Testing
    end
    
    subgraph Pipelines 
        Security
        Test_code[Test code]
        Linters
    end
    Users --Download data--> Extract
    Extract --Flatten data--> Transform
    Transform --Load data into DuckDb--> Load
    Load --Explore data using SQL--> Expose
    Users -.Test.-> Testing
    Extract -.Test.-> Testing
    Transform -.Test.-> Testing
    Load -.Test.-> Testing
    Testing -.Automate.-> Test_code
    Test_code -.-> Security
    Security -.-> Linters
```

## Install pre-requirements

We will use old good `pip` for this use case:

```bash
pip install -r requirements.txt
```

## Create a web app (FastApi)
![LogoFastApi.png](images%2FLogoFastApi.png)
```bash
# Generate Fast api application with default endpoint to return message "Hello DSCAdria!" in json file format
```

Check the app:
* run the app and check default path
```bash
uvicorn main:app --reload

# Check default endpoint
curl http://127.0.0.1:8000

# Check the documentation
http://127.0.0.1:8000/docs
```

## Generate data (Faker)

![LogoFaker.png](images%2FLogoFaker.png)

```bash
# Generate function generate_fake_user to generate fake data for user. 
# A user should contain the following values: id as a unique and primary key, first name, last name and country.
# Use pydantic library for validation and provide lengths, data type and example.

# Create endpoint with name getUser to return 100 records with fake data using function generate_fake_user. 
# Return type should be json

# (optional)    # Generate docstring for the function with an example
```
## Download data (request)
![LogoRequests.png](images%2FLogoRequests.png)
```bash
# Write a procedure download_data to download data from endpoint http://127.0.0.1:8000/getUser
# and save it in json file format. Input parameter is file_name (string data type) and default value is users.json
# Should be saved in the file users.json

# (optional)    # Write a main entry point for the file to call download_data procedure
```

Check the code
```bash
python3 transform_data.py
```

## Transform data (python json -> csv)
![LogoJson.png](images%2FLogoJson.png)
```bash
# Create a procedure load_json to take file_name as input and default value is 'users.json' and load json file. 
# Return value will be a dict data type

# Create a function convert_to_csv to take variable input_json as dict data type as input parameter.
# Function need to flatten dict into flattened list as row
# The return type should be list of list

# Create a procedure save_to_csv to take list as input and file_name as input (default_value is output.csv). 
# Procedure should take input list and save it to csv file
# File need to have first line as a header with columns ID, FIRST_NAME, LAST_NAME, COUNTRY

# Create a function with name load_data where file_name is input parameter with default value 'output.csv'
# Function should load data into duckdb into tables USERS from csv file.
# Create a query to SUM  number of users per country, grouped per column COUNTRY, order by second column and show first 5 records
# Go through the loop where result of query is input and print results
```

## Load data (duckdb)
![LogoDuckDb.png](images%2FLogoDuckDb.png)
```bash
# Create a function with name load_data where file_name is input parameter with default value 'output.csv'
# Function should load data into duckdb into tables USERS from csv file.
# Create a query to SUM number of users per country, grouped per column COUNTRY, order by second column and show first 5 records
# Go through the loop where result of query is input and print results
```
## Show results (SQL)

![LogoSQL.png](images%2FLogoSQL.png)

```bash
# Generate function with name transform_data.
# Function should contain 2 variables: input_file with name 'users.json' and output_file with name 'output.csv'
# Function should call the following procedures:
# download_data
# load_json
# convert_to_csv
# save_to_csv 
# load_data
# After calling these function send a print message 'Transformation completed'
# Surround code block with try catch block and in case or error print message with the error

# (optional)    # Write a main entry point for the file to call transform_data function
```

## Testing the app

## Generate test cases:

![duo_generate_tests.png](images%2Fduo_generate_tests.png)

If you are not happy with the solution, I prefer `pytest` over `unittest`:

```bash
# Generate test cases using pytest library for functions
# download_data
# load_json
# convert_to_csv
# save_to_csv 
# load_data
```

```bash
pytest -vv -x
```

## Refactor the app

![refactor.png](images%2Frefactor.png)

### Explain the code

![explain_code.png](images%2Fexplain_code.png)

## Pipeline run

```bash
# Run app server
uvicorn main:app --reload

# Run the pipeline
python transform_data.py  
```

## Security scan

Security scan is done automatically and it is just a template based solution.

```yaml
stages:
- test
sast:
  stage: test
include:
- template: Security/SAST.gitlab-ci.yml
```

![sast.png](images%2Fsast.png)
## Chat support 

![chat.png](images%2Fchat.png)

```bash
Tell me the way how I can add a security Python-based pipeline in GitLab.
```
```bash
Show me an example of ci pipeline for security testing.
```

```bash
Can you show me of how to access vulnerability report in GitLab?
```
## Summary support

![summary_report.png](images%2Fsummary_report.png)

# 🧪 Discussion

## 🚀 What went well?
1. Can speed up development rapidly and do a many boring thing for us
1. Stick with best practices as able to learn a lot from it
1. Can isolate and improve only piece of problem, which I personally like as a use case 

## 😡What didn’t go well?
1. Domain is still narrow: can't understand the full context nor work with multiple files/packages
1. Still not accurate enough to rumble with large scale problems
1. Prompts should be comprehensive and very details - it is time-consuming 

## 🚀 What we should start doing?
1. Focus on small improvements to make code better
1. Use it as a side by side assistance, not only for coding, but for the entire development process  
1. Use a summary to shorten the understanding process of the issue

## 🛑 What we should stop doing?
1. Put a full control to create modules without a verification
1. Blindly accept all suggestions and create a Frankenstein of your code 

# 🧱 Resources

1. 🎥 [Recorded session](https://youtu.be/Muvn6cPJrmE)
1. Complete code
1. Complete prompts
1. [Slides](https://docs.google.com/presentation/d/1eKMtUj2c_iBu7sSUpbqIa2WwhuctKLRYoBjLeSB7ISI/edit?usp=sharing)

## Further reading

1. [GitLab duo blog](https://about.gitlab.com/blog/2023/09/07/modern-software-development-problems-require-modern-ai-powered-devsecops/)
1. [Learning Python with a little help from AI](https://about.gitlab.com/blog/2023/11/09/learning-python-with-a-little-help-from-ai-code-suggestions/)
1. [glab / glab cli auth](https://docs.gitlab.com/ee/development/gitlab_beta_program/features.html)
1. [10 best practices for using AI-powered GitLab Duo Chat](https://about.gitlab.com/blog/2024/04/02/10-best-practices-for-using-ai-powered-gitlab-duo-chat/)
1. [Boost your productivity with GitLab Duo Chat](https://www.youtube.com/watch?v=RJezT5_V6dI)