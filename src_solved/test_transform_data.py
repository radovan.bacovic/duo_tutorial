import csv
import json
from unittest.mock import mock_open, patch

import pytest  # noqa: F401

from transform_data import (
    convert_to_csv,
    download_data,
    load_json,
    save_to_csv,
)


@pytest.fixture
def mock_response():
    return [
        {"ID": "1", "FIRST_NAME": "John", "LAST_NAME": "Doe", "COUNTRY": "USA"},
        {"ID": "2", "FIRST_NAME": "Jane", "LAST_NAME": "Doe", "COUNTRY": "Canada"},
        {"ID": "3", "FIRST_NAME": "Bob", "LAST_NAME": "Smith", "COUNTRY": "USA"},
    ]


@pytest.fixture
def mock_data():
    return [
        {"ID": "1", "FIRST_NAME": "John", "LAST_NAME": "Doe", "COUNTRY": "USA"},
        {"ID": "2", "FIRST_NAME": "Jane", "LAST_NAME": "Doe", "COUNTRY": "Canada"},
    ]


@pytest.fixture
def tmp_csv_file(tmp_path, mock_response):
    data = convert_to_csv(mock_response)
    output_file = tmp_path / "test.csv"
    save_to_csv(data, output_file)
    return output_file


def test_download_data(mock_response):
    with patch("requests.get") as mock_get:
        mock_get.return_value.json.return_value = mock_response
        mock_get.return_value.raise_for_status = lambda: None

        download_data("test.json")

        with open("test.json", "r") as f:
            data = json.load(f)

        assert data == mock_response


def test_load_json(mock_data):
    with patch("builtins.open", mock_open(read_data=json.dumps(mock_data))):
        data = load_json("test.json")

        assert data == mock_data


def test_convert_to_csv(mock_response):
    expected_output = [
        ["1", "John", "Doe", "USA"],
        ["2", "Jane", "Doe", "Canada"],
        ["3", "Bob", "Smith", "USA"],
    ]

    output = convert_to_csv(mock_response)

    assert output == expected_output


def test_save_to_csv(mock_response, tmp_path):
    data = convert_to_csv(mock_response)

    output_file = tmp_path / "test.csv"

    save_to_csv(data, output_file)

    with open(output_file, "r") as f:
        reader = csv.reader(f)
        header = next(reader)
        rows = list(reader)

    assert header == ["ID", "FIRST_NAME", "LAST_NAME", "COUNTRY"]
    assert rows == data
