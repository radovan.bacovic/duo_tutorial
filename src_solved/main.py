# Generate Fast API application with default endpoint to return a message "Hello DSCAdria!" in JSON format

from typing import List

from faker import Faker
from fastapi import FastAPI
from pydantic import BaseModel, Field

app = FastAPI()
fake = Faker()


# Define User model using Pydantic
class User(BaseModel):
    id: int = Field(..., example=123, description="Unique identifier for the user")
    first_name: str = Field(..., max_length=50, description="First name of the user")
    last_name: str = Field(..., max_length=50, description="Last name of the user")
    country: str = Field(
        ..., max_length=150, description="Country where the user resides"
    )

    class Config:
        schema_extra = {
            "example": {
                "id": 123,
                "first_name": "John",
                "last_name": "Doe",
                "country": "United States",
            }
        }


@app.get("/")
def root():
    return {"message": "Hello DSCAdria!"}


def generate_fake_user() -> User:
    """
    Generate a fake user with the following fields:
    - id: int (unique identifier)
    - first_name: str
    - last_name: str
    - country: str
    """
    user_data = {
        "id": fake.unique.random_int(),
        "first_name": fake.first_name(),
        "last_name": fake.last_name(),
        "country": fake.country(),
    }
    return User(**user_data)


@app.get("/getUser", response_model=List[User])
def get_users():
    """
    Endpoint to return 100 records with fake user data.
    """
    users = [generate_fake_user() for _ in range(100)]
    return users
