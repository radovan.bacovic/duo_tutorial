import csv
import json

import duckdb
import requests


def download_data(file_name="users.json"):
    """
    Download data from the specified endpoint and save it as a JSON file.

    Args:
        file_name (str, optional): The name of the file to save the data. Defaults to 'users.json'.
    """
    url = "http://127.0.0.1:8000/getUser"
    response = requests.get(url)
    response.raise_for_status()  # Raise an exception for non-2xx status codes
    data = response.json()

    with open(file_name, "w") as f:
        json.dump(data, f)


def load_json(file_name="users.json"):
    """
    Load data from a JSON file.

    Args:
        file_name (str, optional): The name of the JSON file to load. Defaults to 'users.json'.

    Returns:
        dict: The loaded data as a dictionary.
    """
    with open(file_name, "r") as f:
        data = json.load(f)
    return data


def convert_to_csv(input_json):
    """
    Convert a list of dictionaries to a list of lists (flattened rows).

    Args:
        input_json (list): A list of dictionaries representing the data.

    Returns:
        list: A list of lists, where each inner list represents a flattened row.
    """
    flattened_list = []
    for item in input_json:
        row = []
        for value in item.values():
            if isinstance(value, dict):
                row.extend(value.values())
            else:
                row.append(value)
        flattened_list.append(row)
    return flattened_list


def save_to_csv(data, file_name="output.csv"):
    """
    Save data to a CSV file.

    Args:
        data (list): A list of lists representing the data to be saved.
        file_name (str, optional): The name of the CSV file to save the data. Defaults to 'output.csv'.
    """
    header = ["ID", "FIRST_NAME", "LAST_NAME", "COUNTRY"]
    with open(file_name, "w", newline="") as f:
        writer = csv.writer(f)
        writer.writerow(header)
        writer.writerows(data)


def load_data(file_name="output.csv"):
    """
    Load data from a CSV file into a DuckDB database, and print the top 5 countries by user count.

    Args:
        file_name (str, optional): The name of the CSV file to load. Defaults to 'output.csv'.
    """
    conn = duckdb.connect()
    conn.execute(f"CREATE TABLE USERS AS SELECT * FROM read_csv_auto('{file_name}')")

    query = """
        SELECT COUNTRY, COUNT(*) AS num_users
        FROM USERS
        GROUP BY COUNTRY
        ORDER BY num_users DESC
        LIMIT 5
    """
    result = conn.execute(query)

    for row in result.fetchall():
        print(row)

    conn.close()


def transform_data(input_file="users.json", output_file="output.csv"):
    """
    Transform data by downloading, converting, and loading it into a DuckDB database.

    Args:
        input_file (str, optional): The name of the input JSON file. Defaults to 'users.json'.
        output_file (str, optional): The name of the output CSV file. Defaults to 'output.csv'.
    """
    try:
        download_data(input_file)
        data = load_json(input_file)
        csv_data = convert_to_csv(data)
        save_to_csv(csv_data, output_file)
        load_data(output_file)
        print("Transformation completed")
    except Exception as e:
        print(f"Error: {e}")


if __name__ == "__main__":
    transform_data()


# Generate test cases using pytest library for functions
# download_data
# load_json
# convert_to_csv
# save_to_csv
# load_data
